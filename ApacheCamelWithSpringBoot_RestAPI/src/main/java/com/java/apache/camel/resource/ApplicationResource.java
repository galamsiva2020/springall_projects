package com.java.apache.camel.resource;

import org.apache.camel.BeanInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.java.apache.camel.dto.Order;
import com.java.apache.camel.processor.OrderProcessor;
import com.java.apache.camel.service.OrderService;


@Component
public class ApplicationResource extends RouteBuilder {

	@Autowired
	private OrderService service;
	
	@BeanInject
	private OrderProcessor  processor;
	@Override
	public void configure() throws Exception {
		//We build the camel component here 
		//bindingMode is expecting the  Json
		// we need enable the camel servlet  in properties file like  below example line
		//camel.component.servlet.mapping.context-path=/*
		restConfiguration().component("servlet").port(9090).host("localhost").bindingMode(RestBindingMode.json);
	
	//We need to expose the RestApi here
		rest().get("/hello-world").produces(MediaType.APPLICATION_JSON_VALUE).route().setBody(constant("welcome  to Apache Camel Application")).endRest();
	
	   
		rest().get("/getOrders").produces(MediaType.APPLICATION_JSON_VALUE).route().setBody(()->service.getOrders()).endRest();
	
	    rest().post("/addOrder").consumes(MediaType.APPLICATION_JSON_VALUE).type(Order.class).outType(Order.class).route().process(processor).endRest();
	}

}
