package com.java.apache.camel.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.java.apache.camel.dto.Order;

@Service
public class OrderService {

	
	private List<Order> list =new ArrayList<>();
	
	//Before executing the main method  execute this one add the list
	@PostConstruct
	public void initDB() {
		
		list.add(new Order(20,"Mobile",30000));
		list.add(new Order(40,"Mobile",40000));
		list.add(new Order(50,"Mobile",60000));
		list.add(new Order(60,"Mobile",70000));
		list.add(new Order(70,"Mobile",80000));
	
	}
	
	public  Order  addOrder(Order order) {
		list.add(order);
		return order;
	}
	
	public List<Order> getOrders(){
		return list;
	}
	
}
