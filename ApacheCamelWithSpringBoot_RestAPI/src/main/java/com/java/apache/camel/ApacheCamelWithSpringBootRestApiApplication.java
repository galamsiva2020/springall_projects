package com.java.apache.camel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApacheCamelWithSpringBootRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApacheCamelWithSpringBootRestApiApplication.class, args);
	}

}
