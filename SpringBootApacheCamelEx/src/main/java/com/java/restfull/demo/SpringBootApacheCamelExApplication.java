package com.java.restfull.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApacheCamelExApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootApacheCamelExApplication.class, args);
	}

}
