package com.java.restfull.demo.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;

public class MyFilesData extends RouteBuilder {

	@Override
	public void configure() throws Exception {
 
		System.out.println("Routing Started");
		//moveAllFiles();
		moveSpecificFile("myFile");
		System.out.println("Routing Ended here");
	}

	
	/*
	 * public void moveAllFiles() {
	 * from("file:C:/inputFolder").to("file:C:/outputFolder"); System.out.
	 * println("Files tranfer one location to another location successfuly");
	 * 
	 * }
	 */
	
	public void moveSpecificFile(String type) {
		from("file:C:/inputFolder").filter(header(Exchange.FILE_NAME).startsWith("myFile")).to("file:C:/outputFolder");
	 System.out.println("myFile only transfer to outputFolder only specifile");
	}
}
