package com.java.apache.camel.paytmservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RequestMapping("/paytmService")
public class PaytmApplication {
	
	@GetMapping("/pay")
	public String paymentProcess() {
		return"payment service called here++++++.......";
	}
	public static void main(String[] args) {
		SpringApplication.run(PaytmApplication.class, args);
	}

}
