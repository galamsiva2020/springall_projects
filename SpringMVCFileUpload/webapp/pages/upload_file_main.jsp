<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="upload_file_main_title" /></title>
</head>
<body>
    <a href="${pageContext.request.contextPath}/upload_file/show_upload_file_form.html">Go To Upload File Form Page</a>
    <br/>
    <form action="${pageContext.request.contextPath}/upload_file/show_upload_file_form.html" method="post">
        <input type="submit" value="Go To Upload File Form Page" />
    </form>
</body>
</html>