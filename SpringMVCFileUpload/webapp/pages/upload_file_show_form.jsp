<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="upload_file_form_title" /></title>
</head>
<body>
    <b>1. Upload Multiple File Example</b>
    <form:form modelAttribute="uploadFileFormData"
        action="${pageContext.request.contextPath}/upload_file/upload_multiple_file.html"
        enctype="multipart/form-data"
        method="Post">
        <input type="file" name="fileOne" /><br/>
        <input type="file" name="fileTwo" /><br/>
        <input type="file" name="fileThree" /><br/><br/>
        <input type="submit" value="Submit"/>
    </form:form>
    <hr/>
    <b>2. Upload One File Example</b>
    <form:form modelAttribute="uploadFileFormData"
        action="${pageContext.request.contextPath}/upload_file/upload_one_file.html"
        enctype="multipart/form-data"
        method="Post">
        <input type="file" name="file" /><br/><br/>
        <input type="submit" value="Submit"/>
    </form:form>
</body>
</html>