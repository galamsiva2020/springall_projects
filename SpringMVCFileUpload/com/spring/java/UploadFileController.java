
@Controller
@RequestMapping("/upload_file")
public class UploadFileController {
    // This is the server side upload file save folder, make sure the folder exist before run the upload file example.
    private static final String UPLOAD_FILE_SAVE_FOLDER = "/tmp/upload_example/";

    /* This method will display the upload file form page. */
    @RequestMapping(value="/show_upload_file_form.html", method= {RequestMethod.GET, RequestMethod.POST})
    public String uploadFileGetForm(Model model) {

        FileUploadFormData fileUploadFormData = new FileUploadFormData();

        model.addAttribute("fileUploadFormData", fileUploadFormData);

        // Return the upload file form page.
        return "upload_file_show_form";
    }

    /* This method will display a page that contains a link and a button which will link to the upload file form page. */
    @RequestMapping(value="/main.html", method= {RequestMethod.GET, RequestMethod.POST})
    public String displayMainPage(Model model) {

        return "upload_file_main";
    }


    /* Process multiple file upload action and return a result page to user. */
    @RequestMapping(value="/upload_multiple_file.html", method={RequestMethod.PUT, RequestMethod.POST})
    public String uploadFileSubmit(Model model, MultipartHttpServletRequest multipartRequest) {
        try
        {
            StringBuffer msgBuf = new StringBuffer();

            // Get multiple file control names.
            Iterator<String> it = multipartRequest.getFileNames();

            while(it.hasNext())
            {
                String fileControlName = it.next();

                MultipartFile srcFile = multipartRequest.getFile(fileControlName);

                String uploadFileName = srcFile.getOriginalFilename();

                // Create server side target file path.
                String destFilePath = UPLOAD_FILE_SAVE_FOLDER+uploadFileName;

                File destFile = new File(destFilePath);

                // Save uploaded file to target.
                srcFile.transferTo(destFile);

                msgBuf.append("Upload file " + uploadFileName + " is saved to " + destFilePath + "<br/><br/>");
            }

            // Set message that will be displayed in return page.
            model.addAttribute("message", msgBuf.toString());

        }catch(IOException ex)
        {
            ex.printStackTrace();
        }finally
        {
            return "upload_file_result";
        }
    }


    /* Process one file upload action and return a result page to user. */
    @RequestMapping(value="/upload_one_file.html", method={RequestMethod.PUT, RequestMethod.POST})
    public String uploadOneFileSubmit(Model model, @ModelAttribute("fileUploadFormData") FileUploadFormData fileUploadFormData, HttpServletRequest req) {
        try
        {
            // Get upload file.
            MultipartFile uploadFile = fileUploadFormData.getFile();

            // Get file name.
            String fileName = uploadFile.getOriginalFilename();

            // Create server side target file path.
            String destFilePath = UPLOAD_FILE_SAVE_FOLDER+fileName;

            File destFile = new File(destFilePath);

            // Save uploaded file to target.
            uploadFile.transferTo(destFile);

            // Set message that will be displayed in return page.
            model.addAttribute("message", "Upload file is saved to '" + destFilePath + "'");

        }catch(IOException ex)
        {
            ex.printStackTrace();
        }finally
        {
            return "upload_file_result";
        }
    }

}
