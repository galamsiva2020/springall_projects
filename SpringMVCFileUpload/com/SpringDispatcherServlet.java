@Configuration
public class SpringDispatcherServlet implements WebApplicationInitializer {
	
	@Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        AnnotationConfigWebApplicationContext dispatcherServletContext = new AnnotationConfigWebApplicationContext();

        dispatcherServletContext.register(SpringConfig.class);

        DispatcherServlet dispatcherServlet = new DispatcherServlet(dispatcherServletContext);

        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("file-upload", dispatcherServlet);
         
        /* If do not set multipartconfig in servlet 3, when you upload a file, it will throw Unable to process parts as no multi-part configuration has been provided error. */
        MultipartConfigElement multipartConfig = new MultipartConfigElement("/tmp");
        dispatcher.setMultipartConfig(multipartConfig);

        dispatcher.setLoadOnStartup(1);

        dispatcher.addMapping("*.html");

        FilterRegistration.Dynamic multipartFilter = servletContext.addFilter("multipartFilter", MultipartFilter.class);

        multipartFilter.addMappingForUrlPatterns(null, true, "/*");
    }

}
