package com.java.restfull.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSecurityRestExample1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSecurityRestExample1Application.class, args);
	}

}
