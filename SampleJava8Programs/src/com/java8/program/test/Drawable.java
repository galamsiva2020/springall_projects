package com.java8.program.test;

public interface Drawable {
	
	public void draw();

}
