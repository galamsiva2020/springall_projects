package com.java8.program.test;

public class LambdaExpressionExample2 {

	public static void main(String[] args) {

		int width =20;
		
		//without Lambda expression below code
		//without lambda, Drawable implementation using anonymous class  
		Drawable d=new Drawable(){  
			public void draw(){System.out.println("without lambda expressions using Drawing "+width);}  
		};  
		d.draw(); 
		
		
		//With lambada expression using here
		Drawable d2 =() ->{
			System.out.println(" with lambda expressions using Drawing the "+width);
		};
		d2.draw();
		
	}

}
