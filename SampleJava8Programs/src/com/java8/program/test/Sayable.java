package com.java8.program.test;

public interface Sayable {
	public void say();// abstract method
	
	//Any number of Object class methods contain here no worries
	
	int hashCode();
	String  toString();
	boolean equals(Object obj);
	
	

}
