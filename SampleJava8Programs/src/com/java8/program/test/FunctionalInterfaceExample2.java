package com.java8.program.test;

public class FunctionalInterfaceExample2 implements  Sayable {


	public static void main(String[] args) {
		FunctionalInterfaceExample2 fie = new FunctionalInterfaceExample2();
		fie.say();
	}

	@Override
	public void say() {
     System.out.println(" This is the Sayable interface abstract method implementation here");		
	}

}
