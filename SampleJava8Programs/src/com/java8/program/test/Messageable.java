package com.java8.program.test;

public interface Messageable {
	Message getMessage(String msg);

}
