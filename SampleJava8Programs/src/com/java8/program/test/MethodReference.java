package com.java8.program.test;

public class MethodReference {

	public static void saySomething() {
		System.out.println("Hello this is static method of this program");
		
	}
	public static void main(String[] args) {

		//Referring static method
		Sayable sayable = MethodReference::saySomething;
		//Calling interface method here
		System.out.println("This is interface method calling here");
		sayable.say();
		/*
		 * sayable.display(); sayable.draw();
		 */
		
	}

}
