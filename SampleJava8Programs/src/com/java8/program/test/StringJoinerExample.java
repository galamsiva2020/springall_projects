package com.java8.program.test;

import java.util.StringJoiner;

public class StringJoinerExample {

	public static void main(String[] args) {
		
		//Java8  one of feature StringJoiner  is class introduced java8
		//StringJoiner joinnames = new StringJoiner(","); //passing comma(,) as delimiter
		
        StringJoiner joinnames = new StringJoiner(",", "[", "]");   // passing comma(,) and square-brackets as delimiter   

		//Adding  values  to StringJoiner
		joinnames.add("Jesus");
		joinnames.add("Ramesh Chinta");
		joinnames.add("Sarada");
		joinnames.add("Parvathi");
		joinnames.add("Sivakrishna");
		joinnames.add("vinesh sister");
		joinnames.add("stephenJoin");
		joinnames.add("John puspa");
		joinnames.add("Santosh");
		joinnames.add("Hosanna");
		joinnames.add("johnson");
		
		System.out.println(joinnames);
		
		

	}

}
