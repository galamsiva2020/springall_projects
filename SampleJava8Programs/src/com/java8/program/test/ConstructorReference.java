package com.java8.program.test;

public class ConstructorReference {

	public static void main(String[] args) {

		Messageable hello = Message::new;
		hello.getMessage("This is Constructor refrence");
	}

}
