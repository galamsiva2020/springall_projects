package com.java8.program.test;

import java.util.Optional;

public class OptionalExampleNullpointerMainTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String[] str = new String[10];
		//If we assign the value then display value if not then execute the else block and display value
		str[5] ="Sivakrishna Optional class example here"; //setting value  for 5th index
		
		Optional<String> checkNull = Optional.ofNullable(str[5]);
		if(checkNull.isPresent()) { //Check for value is present or not
			String lowercaseString =str[5].toLowerCase();
			System.out.println(lowercaseString);
		}else
			System.out.println("String value is not present there");
 
		
	}

}
