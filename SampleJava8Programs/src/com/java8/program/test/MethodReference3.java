package com.java8.program.test;

import java.util.function.BiFunction;

public class MethodReference3 {
	
	public static void main(String[] args) {
		//BiFunction is a functional interface
		//Type Parameters:
		//<T> the type of the first argument to the function
		//<U> the type of the second argument to the function
		//<R> the type of the result of the function
		BiFunction<Integer,Integer,Integer> adder = Arithmetic:: add;
		//apply is the  method of the BiFunction interface
		int result=adder.apply(20, 48);
		System.out.println(result);
		
	}

}
