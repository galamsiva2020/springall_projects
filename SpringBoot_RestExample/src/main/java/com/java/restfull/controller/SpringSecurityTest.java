package com.java.restfull.controller;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SpringSecurityTest extends WebSecurityConfigurerAdapter {
	
	
 protected void configure(AuthenticationManagerBuilder auth)throws Exception {
	 
	 auth.inMemoryAuthentication()
	 .withUser("sivakrishna")
	 .password("sivakrishna")
	 .roles("USER","ADMIN");
	 
 }
 
	/*
	 * protected void configure(HttpSecurity http) throws Exception{
	 * 
	 * http //http basic authentication .httpBasic() .and() .authorizeRequests()
	 * .antMatchers(HttpMethod.GET, "/student/allstudent**").hasRole("USER")
	 * .antMatchers(HttpMethod.POST, "/register/student**").hasRole("USER")
	 * 
	 * .antMatchers(HttpMethod.PUT, "/books/**").hasRole("ADMIN")
	 * .antMatchers(HttpMethod.DELETE, "/books/**").hasRole("ADMIN")
	 * .antMatchers(HttpMethod.PATCH, "/books/**").hasRole("ADMIN") .and()
	 * .csrf().disable() .formLogin().disable();
	 * 
	 * }
	 */ 
 }
	
