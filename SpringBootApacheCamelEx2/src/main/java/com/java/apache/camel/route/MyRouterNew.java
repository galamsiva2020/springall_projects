package com.java.apache.camel.route;

import java.util.StringTokenizer;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;
@Component
public class MyRouterNew extends RouteBuilder {

	@Override
	public void configure() throws Exception {
      from("file:D:/source")
      .process(new Processor() {
		public void process(Exchange exchange)
				throws Exception {
			// read input message given by source
			Message input= exchange.getIn();
			//read body  as string from  Input message
			String data =input.getBody(String.class);
			
			//operation
			StringTokenizer  str=  new StringTokenizer(data,",");
			String  eid = str.nextToken();
			String  ename = str.nextToken();
			String  esal = str.nextToken();
			//output message
			String  dataModified ="{eid:"+eid+",ename:"+ename+",esal:"+esal+"}";
			
			//read output  message reference
			//Message  output = exchange.getOut(); it is deprecated
			Message output =exchange.getMessage();
			
			//set data  to output
			output.setBody(dataModified);
		}
	})
      .to("file:D:/desti?fileName=emp.json");
	}

}

/* By using Lambda expressions here
 * @Override
	public void configure() throws Exception {
      from("file:E:/source")
      .process(
               (exchange)-> {
			// read input message given by source
			Message input= exchange.getIn();
			//read body  as string from  Input message
			String data =input.getBody(String.class);
			
			//operation
			StringTokenizer  str=  new StringTokenizer(data,",");
			String  eid = str.nextToken();
			String  ename = str.nextToken();
			String  esal = str.nextToken();
			//output message
			String  dataModified ="{eid:"+eid+",ename:"+ename+",esal:"+esal+"}";
			
			//read output  message reference
			//Message  output = exchange.getOut(); it is deprecated
			Message output =exchange.getMessage();
			
			//set data  to output
			output.setBody(dataModified);
		}
	})
      .to("file:E:/desti?fileName=emp.json");
	}

 */
