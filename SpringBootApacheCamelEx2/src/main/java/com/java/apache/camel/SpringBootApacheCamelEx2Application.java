package com.java.apache.camel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApacheCamelEx2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootApacheCamelEx2Application.class, args);
	}

}
