package com.java.test;

public class FizzBuzzMainTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n=15;
		
		for(int i=1; i<=n; i++) {
			if(i% 15 ==0)
				System.out.println("FizzBuzz"+" ");
			else if(i%5 ==0)
				System.out.println("Buzz"+" ");
			else if(i%3 ==0)
				System.out.println("Fuzz"+" ");
			else
				System.out.println(i+" ");
		}
	}

}
