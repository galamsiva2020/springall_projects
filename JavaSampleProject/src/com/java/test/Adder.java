package com.java.test;

public class Adder {
	//Change the return type of Method in Method Overloading is not possible example
	/*
	 * static int add(int a,int b){return a+b;} static double add(int a,int
	 * b){return a+b;}
	 */

	//Multi catch block exceptions in Java example
	 public static void main(String[] args) {  
         
         try{    
              int a[]=new int[5];    
              
              System.out.println(a[3]);  
             }    
             catch(ArithmeticException e)  
                {  
                 System.out.println("Arithmetic Exception occurs");  
                }    
             catch(ArrayIndexOutOfBoundsException e)  
                {  
                 System.out.println("ArrayIndexOutOfBounds Exception occurs");  
                }    
             catch(Exception e)  
                {  
                 System.out.println("Parent Exception occurs");  
                }             
             System.out.println("rest of the code");    
  }
}
