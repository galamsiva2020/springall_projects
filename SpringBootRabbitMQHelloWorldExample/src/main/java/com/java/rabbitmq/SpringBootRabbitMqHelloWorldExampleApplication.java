package com.java.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRabbitMqHelloWorldExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRabbitMqHelloWorldExampleApplication.class, args);
	}

}
